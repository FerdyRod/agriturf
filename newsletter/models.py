from django.db import models

class Subscriber(models.Model):
    email = models.EmailField(max_length=254)
    created = models.DateTimeField(auto_now_add=True)
    subscribed = models.BooleanField(default=True)

    def __unicode__(self):
        return "(%s) %s" % (self.subscribed_indicator, self.email)

    @property
    def subscribed_indicator(self):
        return "+" if self.subscribed else "-"