from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest
from django.template import Template
from forms import SubscriberForm
from models import Subscriber

def subscribe(request):
    success = False
    if request.method == 'POST':
        form = SubscriberForm(data=request.POST)
        # validate form data
        if form.is_valid():
            s = Subscriber.objects.update_or_create(email=form.cleaned_data['email'], 
                                                    defaults = {"subscribed": True})
            success = True
        else:
            return HttpResponseBadRequest("Invalid Email Address Format")
    return render(request, 'newsletter/subscribe.html',{"success":success})


def unsubscribe(request):
    success = False
    if request.method == 'POST':
        form = SubscriberForm(data=request.POST)
        try:
            if form.is_valid():
                s = Subscriber.objects.get(email=form.cleaned_data['email'])
                s.subscribed = False
                s.save()
                success=True
            else:
                # The user has turned off JS and is trying to attack.
                return HttpResponseBadRequest("Invalid Email Address Format")

        except Subscriber.DoesNotExist:
            # if we can't pull a user from the DB, just fail silently and
            # give same response as a succesful unsubscribe
            success=True

    return render(request, 'newsletter/unsubscribe.html', {"success":success})
