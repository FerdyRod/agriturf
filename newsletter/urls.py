from django.conf.urls import patterns, include, url
import views

urlpatterns = patterns('agriturf_supplies.services.views',

    url(r'^subscribe/$', views.subscribe, name='subscribe'),
    url(r'^unsubscribe/$', views.unsubscribe, name='unsubscribe'),

)