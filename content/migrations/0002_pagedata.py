# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0001_squashed_0003_auto_20150211_0508'),
    ]

    operations = [
        migrations.CreateModel(
            name='PageData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('page', models.CharField(max_length=50)),
                ('section', models.CharField(max_length=50)),
                ('sort', models.IntegerField(default=0)),
                ('internal_description', models.CharField(max_length=1000, blank=True)),
                ('data_1', models.TextField(blank=True)),
                ('data_2', models.TextField(blank=True)),
                ('data_3', models.TextField(blank=True)),
                ('data_4', models.TextField(blank=True)),
                ('data_5', models.TextField(blank=True)),
                ('image_1', models.FileField(upload_to=b'services/images/', blank=True)),
                ('image_2', models.FileField(upload_to=b'services/images/', blank=True)),
                ('image_3', models.FileField(upload_to=b'services/images/', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
