# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    replaces = [(b'content', '0001_initial'), (b'content', '0002_auto_20150210_0526'), (b'content', '0003_auto_20150211_0508')]

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ActionEmail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email_address', models.CharField(max_length=200)),
                ('event', models.CharField(max_length=50)),
                ('internal_description', models.CharField(max_length=2000)),
                ('subject', models.CharField(max_length=200)),
                ('body', models.CharField(max_length=10000)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Application',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=1000)),
                ('email', models.CharField(max_length=1000)),
                ('phone', models.CharField(max_length=50)),
                ('department', models.CharField(max_length=100)),
                ('resume', models.FileField(upload_to=b'/home/jason/Desktop/agriturf_supplies/static/resume_upload')),
                ('cover_letter', models.CharField(max_length=5000)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=1000)),
                ('description', models.CharField(max_length=5000)),
                ('required_experience', models.CharField(max_length=1000, blank=True)),
                ('work_type', models.CharField(max_length=100, blank=True)),
                ('department', models.CharField(max_length=100)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('active', models.BooleanField(default=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('email', models.CharField(max_length=200)),
                ('phone', models.CharField(max_length=50, blank=True)),
                ('message', models.CharField(max_length=200)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='application',
            name='job',
            field=models.ForeignKey(to='content.Job'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='application',
            name='resume',
            field=models.FileField(upload_to=b'resume_upload/'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='application',
            name='cover_letter',
            field=models.CharField(max_length=5000, blank=True),
            preserve_default=True,
        ),
    ]
