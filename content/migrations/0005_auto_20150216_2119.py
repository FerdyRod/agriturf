# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0004_auto_20150212_0639'),
    ]

    operations = [
        migrations.AddField(
            model_name='application',
            name='eligible',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='job',
            name='required_experience',
            field=models.TextField(),
            preserve_default=True,
        ),
    ]
