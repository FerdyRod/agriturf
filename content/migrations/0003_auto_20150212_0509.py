# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0002_pagedata'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='pagedata',
            options={'verbose_name_plural': 'page data'},
        ),
    ]
