from content.models import PageData

def load_pagedata(request):
    if request.resolver_match.app_name:
        page = "%s:%s" % (request.resolver_match.app_name, request.resolver_match.url_name)
    else:
        page = "%s" % (request.resolver_match.url_name)
    pagedata = PageData.objects.filter(page=page)
    return {'pagedata':pagedata}