from django.db import models
from django.conf import settings
from itertools import groupby


class Job(models.Model):
    title = models.CharField(max_length=1000)
    description = models.CharField(max_length=5000)
    required_experience = models.TextField()
    work_type = models.CharField(max_length=100, blank=True)
    department = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)

    def __unicode__(self):
        return "(%s) %s - %s" % (self.active_indicator, self.department, self.title)

    @property
    def active_indicator(self):
        return "+" if self.active else "-"


class Application(models.Model):
    job = models.ForeignKey('Job')
    name = models.CharField(max_length=1000)
    email = models.CharField(max_length=1000)
    phone = models.CharField(max_length=50)
    department = models.CharField(max_length=100)
    resume = models.FileField(upload_to="resume_upload/")
    cover_letter = models.CharField(max_length=5000, blank=True)
    eligible = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return "%s - %s" % (self.created.strftime('%m/%d/%y'), self.name)


class ActionEmail(models.Model):
    email_address = models.CharField(max_length=200)
    event = models.CharField(max_length=50)
    internal_description = models.CharField(max_length=2000)
    subject = models.CharField(max_length=200)
    body = models.CharField(max_length=10000)

    def __unicode__(self):
        return self.event


class Message(models.Model):
    name = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    phone = models.CharField(max_length=50, blank=True)
    message = models.CharField(max_length=200)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return "%s - %s - %s" % (self.created.strftime('%m/%d/%y'), self.name, self.message)


class PageData(models.Model):

    page = models.CharField(max_length=50)
    section = models.CharField(max_length=50)
    sort = models.IntegerField(default=0)
    internal_description = models.CharField(max_length=1000, blank=True)
    data_1 = models.TextField(blank=True)
    data_2 = models.TextField(blank=True)
    data_3 = models.TextField(blank=True)
    data_4 = models.TextField(blank=True)
    data_5 = models.TextField(blank=True)
    image_1 = models.FileField(upload_to='services/images/', blank=True)
    image_2 = models.FileField(upload_to='services/images/', blank=True)
    image_3 = models.FileField(upload_to='services/images/', blank=True)

    def __unicode__(self):
        return "%s - %s" % (self.page, self.section)

    @classmethod
    def get_page_content(cls, page):
        content = cls.objects.order_by('section','sort').filter(page=page).values()
        content_tuple = groupby(content, lambda x : x['section'])
        content_dict = dict((k,list(v)) for k,v in content_tuple)
        return content_dict

    class Meta:
        verbose_name_plural = "page data"

