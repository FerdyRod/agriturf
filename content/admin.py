from django.contrib import admin
from models import Job, Application, ActionEmail, Message, PageData

@admin.register(Job)
class JobAdmin(admin.ModelAdmin):
    pass

@admin.register(Application)
class ApplicationAdmin(admin.ModelAdmin):
    pass

@admin.register(ActionEmail)
class ActionEmailAdmin(admin.ModelAdmin):
    pass

@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    pass

@admin.register(PageData)
class PageDataAdmin(admin.ModelAdmin):
    pass