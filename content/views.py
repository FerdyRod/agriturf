from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest
from django.template import Template

from forms import MessageForm, ApplicationForm
from models import Job, PageData
from services.models import Service

def home(request):
    featured = Service.objects.filter(is_featured_solution=True)
    return render(request, 'content/home.html', {"page":"home", 'featured':featured})

def about(request):
    return render(request, 'content/about.html', {"page":"about"})

def careers(request):
    # submit career
    if request.method == 'POST':
        form = ApplicationForm(request.POST, request.FILES)
        if form.is_valid():
            jobapplication = form.save(commit=False)
            jobapplication.save()
            return render(request, 'content/submit_application.html', {'success': True})
        else:
            return HttpResponseBadRequest("An error occured while processing your request - %s" % form.errors)
    # generate page data
    jobs = Job.objects.filter(active=True)
    return render(request, 'content/careers.html', {'jobs':jobs})

def terms_of_use(request):
    return render(request, 'content/terms_of_use.html', {})

def send_message(request):
    success = False
    if request.method == 'POST':
        form = MessageForm(request.POST)
        if form.is_valid():
            message = form.save(commit=False)
            message.save()
            success = True
        else:
            ## since we are using JS validation, if we receive bad data, then
            ## the user is doing something strange so just error out.
            return HttpResponseBadRequest("An error occured while sending your message.")
    return render(request, 'content/send_message.html', {'success': success, 'page':'contact'})
