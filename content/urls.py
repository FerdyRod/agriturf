from django.conf.urls import patterns, include, url
import views

urlpatterns = patterns('agriturf_supplies.content.views',

    #(r'^index/$', views.home), # main urls.py maps this directly to /
    url(r'^about/$', views.about, name='about'),
    url(r'^careers/$', views.careers, name='careers'),
    url(r'^terms_of_use/$', views.terms_of_use, name='terms_of_use'),
    url(r'^send_message/$', views.send_message, name='send_message'),
)