from django.forms import ModelForm
from django import forms

from models import Message, Application


class MessageForm(ModelForm):
    class Meta:
        model = Message
        fields = ['name', 'email', 'phone', 'message']


class ApplicationForm(ModelForm):

    def clean(self):
        clean_data = super(ApplicationForm, self).clean()
        resume = clean_data.get('resume')
        ext = resume.name.split('.')[-1]
        ext_list = ['pdf', 'txt', 'doc', 'docx']
        if ext.lower() not in ext_list:
            raise forms.ValidationError("The file you are trying to upload is not permitted.")
        return clean_data

    class Meta:
        model = Application
        fields = ['job', 'name', 'email', 'phone', 'resume', 'cover_letter', 'eligible']
