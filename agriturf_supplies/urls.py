from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'agriturf_supplies.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'content.views.home', name='home'),
    (r'^content/', include('content.urls', app_name='content', namespace="content")),
    (r'^services/', include('services.urls', app_name='services', namespace="services")),
    (r'^shop/', include('shop.urls', app_name='shop', namespace="shop")),
    (r'^newsletter/', include('newsletter.urls', app_name='newsletter', namespace="newsletter")),

)

# don't run static files in production!!
if settings.DEBUG:
    urlpatterns += patterns('django.contrib.staticfiles.views',
            url(r'^static/(?P<path>.*)$', 'serve'),
        )

    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes': False}),
    )

# Configuration for admin Title:
admin.site.site_header = 'Agriturf Administration'