from django.conf.urls import patterns, include, url
import views

urlpatterns = patterns('agriturf_supplies.services.views',

    #(r'^index/$', views.home), # main urls.py maps this directly to /
    url(r'^product/(?P<slug>.*)$', views.product, name='view_product'),
    url(r'^cart/$', views.cart, name='cart'),
    url(r'^checkout/$', views.checkout, name='start_checkout'),
    url(r'^checkout/(\d+)$', views.checkout, name='checkout_step'),

)
