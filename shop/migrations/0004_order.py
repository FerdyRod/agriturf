# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0005_auto_20150214_0252'),
        ('shop', '0003_auto_20150212_0543'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order_number', models.IntegerField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('order_total', models.DecimalField(default=0, max_digits=6, decimal_places=2)),
                ('send_labels', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=255)),
                ('email', models.EmailField(max_length=75)),
                ('phone', models.CharField(max_length=255, blank=True)),
                ('address1', models.CharField(max_length=255, blank=True)),
                ('address2', models.CharField(max_length=255, blank=True)),
                ('city', models.CharField(max_length=255, blank=True)),
                ('state', models.CharField(max_length=2, blank=True)),
                ('zip_code', models.CharField(max_length=5, blank=True)),
                ('number_of_products', models.IntegerField(null=True, blank=True)),
                ('for_pickup', models.BooleanField(default=False)),
                ('scheduled', models.DateField(null=True, blank=True)),
                ('order_type', models.CharField(max_length=15)),
                ('service', models.ForeignKey(blank=True, to='services.Service', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
