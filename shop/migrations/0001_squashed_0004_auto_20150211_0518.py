# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    replaces = [(b'shop', '0001_initial'), (b'shop', '0002_auto_20150211_0510'), (b'shop', '0003_auto_20150211_0514'), (b'shop', '0004_auto_20150211_0518')]

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=500)),
                ('slug', models.CharField(max_length=500, blank=True)),
                ('description', models.TextField()),
                ('size', models.CharField(max_length=255, blank=True)),
                ('label', models.FileField(upload_to=b'products/product_labels/', blank=True)),
                ('msds_label', models.FileField(upload_to=b'products/msds_labels/', blank=True)),
                ('product_price', models.DecimalField(max_digits=5, decimal_places=2)),
                ('sale_price', models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)),
                ('on_sale', models.BooleanField(default=False)),
                ('organic', models.BooleanField(default=False)),
                ('image_1', models.FileField(upload_to=b'products/images/')),
                ('image_2', models.FileField(upload_to=b'products/images/', blank=True)),
                ('image_3', models.FileField(upload_to=b'products/images/', blank=True)),
                ('image_4', models.FileField(upload_to=b'products/images/', blank=True)),
                ('image_5', models.FileField(upload_to=b'products/images/', blank=True)),
                ('manufacturer_name', models.CharField(max_length=255)),
                ('manufacturer_website', models.URLField(max_length=255, blank=True)),
                ('rate_combo', models.CharField(max_length=255, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
