# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def generate_slugs(apps, schema_editor):
    Product = apps.get_model("shop", "Product")
    products = Product.objects.all()
    for p in products:
        p.save()

class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0005_order_products'),
    ]

    operations = [
        migrations.RunPython(generate_slugs)
    ]
