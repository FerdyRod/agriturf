from django import forms

class CheckoutStepOne(forms.Form):
    name = forms.CharField()
    email = forms.CharField()
    phone = forms.CharField()
    address1 = forms.CharField()
    address2 = forms.CharField(required=False)
    city = forms.CharField()
    state = forms.CharField()
    zip_code = forms.CharField()

class AdjustCartQuantity(forms.Form):
    action = forms.CharField()
    quantity = forms.IntegerField()
    item_number = forms.IntegerField()

    def clean_action(self):
        if not self.cleaned_data['action'] == "adjust_quantity":
            raise forms.ValidationError("Invalid Action")