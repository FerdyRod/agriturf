import itertools

from django.db import models
from django.utils.text import slugify
from random import randint


class Product(models.Model):

    name = models.CharField(max_length=500)
    slug = models.CharField(max_length=500, blank=True, null=True)
    description = models.TextField()
    size = models.CharField(max_length=255, blank=True)
    label = models.FileField(upload_to='products/product_labels/', blank=True)
    msds_label = models.FileField(upload_to='products/msds_labels/', blank=True)
    product_price = models.DecimalField(max_digits=5, decimal_places=2)
    sale_price = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    on_sale = models.BooleanField(default=False)
    organic = models.BooleanField(default=False)
    image_1 = models.FileField(upload_to='products/images/')
    image_2 = models.FileField(upload_to='products/images/', blank=True)
    image_3 = models.FileField(upload_to='products/images/', blank=True)
    image_4 = models.FileField(upload_to='products/images/', blank=True)
    image_5 = models.FileField(upload_to='products/images/', blank=True)
    manufacturer_name = models.CharField(max_length=255)
    manufacturer_website = models.URLField(max_length=255, blank=True)
    rate_combo = models.CharField(max_length=255, blank=True)

    @property
    def current_price(self):
        if self.on_sale:
            return self.sale_price
        else:
            return self.product_price

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        """
        Creating unique slug on save by adding an integer if it
        already exists.
        """
        self.slug = original = slugify(self.name)
        for x in itertools.count(2):
            if not Product.objects.filter(slug=self.slug).exists():
                break
            self.slug = '%s-%d' % (original, x)
        super(Product, self).save(*args, **kwargs)


class Order(models.Model):

    order_number = models.IntegerField()
    order_type = models.CharField(max_length=15)
    for_pickup = models.BooleanField(default=False)
    scheduled = models.DateField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    order_total = models.DecimalField(max_digits=6, decimal_places=2, default=0)
    number_of_products = models.IntegerField(blank=True, null=True)
    service = models.ForeignKey('services.service', blank=True, null=True)
    send_labels = models.BooleanField(default=False)
    #description = models.TextField()
    name = models.CharField(max_length=255)
    email = models.EmailField()
    phone = models.CharField(max_length=255, blank=True)
    address1 = models.CharField(max_length=255, blank=True)
    address2 = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255, blank=True)
    state = models.CharField(max_length=2, blank=True)
    zip_code = models.CharField(max_length=5, blank=True)
    products = models.ManyToManyField('product')

    def save(self, *args, **kwargs):
        # generate random order number not in db
        while not self.order_number:
            random_number = randint(100000,999999)
            if not Order.objects.filter(order_number=random_number):
                self.order_number = random_number
        super(Order, self).save(*args, **kwargs)

    def __unicode__(self):
        return "%s - Order # %s - %s" % (self.created.strftime("%m.%d.%y"), self.order_number, self.name)


class Cart(object):
    def __init__(self, request):
        self.request = request
        # init cart session object if not set
        if not request.session.get('cart'):
            request.session['cart'] = {}
        self.cart = self.request.session['cart']

    def adjust(self,item, quantity):
        item = str(item)
        quantity = int(quantity)

        current_quantity = self.get_item_quantity_in_cart(item)
        self.cart[item] = current_quantity + quantity

        self.clean_cart()
        self.set_cart_quantity()
        self.save_to_session()

    def get_item_quantity_in_cart(self,item):
        try:
            return self.cart[item]
        except KeyError:
            return 0

    @property
    def items(self):
        self.clean_cart()
        self.set_cart_quantity()
        return self.quantity

    def set_cart_quantity(self):
        quantity = sum(v for k, v in self.cart.items())
        self.quantity = quantity

    def clean_cart(self):
        # cleans out items with quantity of 0
        for k,v in self.cart.items():
            if v <= 0:
                self.cart.pop(k)

    def save_to_session(self):
        self.request.session['cart'] = self.cart
        self.request.session['cart_quantity'] = self.quantity
        self.request.session['cart_subtotal'] = Cart.get_product_total_cost(self.request)

    @classmethod
    def get_cart_contents(cls, request):
        cart_items = []
        item_ids = [k for k in request.session['cart']]
        products = Product.objects.filter(pk__in=item_ids)

        for k,v in request.session['cart'].items():
            obj = [ i for i in products if i.id==int(k)][0]
            item = {'product': obj, 'quantity': v, 'price_of_quantity': obj.current_price * v }
            cart_items.append(item)

        return cart_items

    @classmethod
    def get_product_total_cost(cls, request):
        cart_items = cls.get_cart_contents(request)
        subtotal = 0.00
        for item in cart_items:

            product = item["product"].current_price * item['quantity']
            subtotal += float(product)
        return subtotal

