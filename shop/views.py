from django.shortcuts import render
from django.http import HttpResponse
from django.template import Template
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from models import Product, Cart
from forms import CheckoutStepOne, AdjustCartQuantity

# Create your views here.

def product(request, slug):
    product = Product.objects.get(slug=slug)
    savings = 100 - (product.sale_price / product.product_price)*100

    return render(request, 'shop/product.html', {"product":product, 'savings':savings})

def cart(request):
    cart = Cart(request)

    # user is adjusting quantity
    adjust_cart_quantity = AdjustCartQuantity(request.POST)
    if adjust_cart_quantity.is_valid():
        cart.adjust(adjust_cart_quantity.cleaned_data['item_number'], adjust_cart_quantity.cleaned_data['quantity'])

    cart_items = Cart.get_cart_contents(request)

    return render(request, 'shop/cart.html', {'page':'cart', 'cart_items':cart_items})

def checkout(request, step=1):
    ##bounce user back to cart if no products in cart
    if not Cart(request).items:
        return redirect(reverse('shop:cart'))

    step = int(step)
    if step == 1:
        cart_items = Cart.get_cart_contents(request)
        #cart_subtotal = Cart.get_product_total_cost(request)

        return render(request, 'shop/checkout_step_1.html', {'page':'cart', 'cart_items':cart_items})

    elif step == 2:
        form = CheckoutStepOne(request.POST)
        if form.is_valid():
            # save form data to session
            request.session['user_info'] = form.cleaned_data
        else:
            return redirect(reverse('shop:start_checkout'))

        return render(request, 'shop/checkout_step_2.html', {'page':'cart'})

