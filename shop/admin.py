from django.contrib import admin
from models import Product, Order


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    exclude = ('slug',)


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    pass
