from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest
from django.template import Template
from django.shortcuts import render
from models import Category, Service
from forms import OrderForm
from django.db.models import Q
from django.core.paginator import EmptyPage, Paginator, PageNotAnInteger

# Create your views here.


def view(request, slug):
    success = False
    if request.method == 'POST':
        form = OrderForm(request.POST)
        if form.is_valid():
            success = True
        else:
            return HttpResponseBadRequest("A error ocurred while processing your request - %s" % form.errors)
    service = Service.objects.get(slug=slug)
    return render(request, 'services/view.html', {"service": service, 'success': success})


def schedule(request):
    return render(request, 'services/schedule.html', {})


def search(request, category=None):
    if category:
        category = Category.objects.get(pk=category)
        selected = category
    else:
        category = Category.objects.get(pk=1)
        selected = None

    cat_ids = category.get_category_ids()
    print cat_ids
    services = Service.objects.filter(category_id__in=cat_ids)
    paginator = Paginator(services, 9)

    page = request.GET.get('page')
    try:
        services = paginator.page(page)
    except PageNotAnInteger:
        services = paginator.page(1)
    except EmptyPage:
        services = paginator.page(paginator.num_pages)

    menu = Category.get_menu(category.id)

    return render(request, 'services/search.html', {'page': 'solutions', 'menu': menu, 'services': services, 'selected': selected})
