# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0006_service_related_products'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='related_services',
            field=models.ManyToManyField(related_name='related_services_rel_+', to='services.Service'),
            preserve_default=True,
        ),
    ]
