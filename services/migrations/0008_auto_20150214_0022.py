# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0007_service_related_services'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='related_products',
            field=models.ManyToManyField(to='shop.Product', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='service',
            name='related_services',
            field=models.ManyToManyField(related_name='related_services_rel_+', null=True, to='services.Service', blank=True),
            preserve_default=True,
        ),
    ]
