# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.core.management import call_command
import os

fixture_dir = os.path.abspath(os.path.join(os.path.dirname(__file__)))
fixture_filename = 'services_test_data.json'


def load_fixture(apps, schema_editor):
    fixture_file = os.path.join(fixture_dir, fixture_filename)
    call_command('loaddata', fixture_file)

def unload_fixture(apps, schema_editor):
    pass
  

class Migration(migrations.Migration):

    dependencies = [
        ('services', '0003_auto_20150212_0509'),
    ]

    operations = [
        migrations.RunPython(load_fixture, reverse_code=unload_fixture)
    ]
