# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

from services.models import Service

def generate_slugs(apps, schema_editor):
    #Service = apps.get_model("services", "Service") # for some reason it would not work, so imported model directly
    services = Service.objects.all()
    for s in services:
        s.save()

def migrate_down(apps, schema_editor):
    pass

class Migration(migrations.Migration):

    dependencies = [
        ('services', '0010_auto_20150214_1318'),
    ]

    operations = [
        migrations.RunPython(generate_slugs, migrate_down)
    ]
