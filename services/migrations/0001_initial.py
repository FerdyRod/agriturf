# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('parent', models.ForeignKey(related_name='subcategories', blank=True, to='services.Category', null=True)),
            ],
            options={
                'verbose_name_plural': 'categories',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=500)),
                ('slug', models.CharField(max_length=500)),
                ('description', models.TextField()),
                ('identification_guide', models.TextField()),
                ('image_1', models.FileField(upload_to=b'services/images/')),
                ('image_2', models.FileField(upload_to=b'services/images/', blank=True)),
                ('image_3', models.FileField(upload_to=b'services/images/', blank=True)),
                ('image_4', models.FileField(upload_to=b'services/images/', blank=True)),
                ('image_5', models.FileField(upload_to=b'services/images/', blank=True)),
                ('is_featured_solution', models.BooleanField(default=False)),
                ('featured_solution_weight', models.IntegerField(default=5)),
                ('category', models.ForeignKey(to='services.Category')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
