# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0005_order_products'),
        ('services', '0005_auto_20150214_0252'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='related_products',
            field=models.ManyToManyField(to='shop.Product'),
            preserve_default=True,
        ),
    ]
