# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.core.management import call_command
import os

fixture_dir = os.path.abspath(os.path.join(os.path.dirname(__file__)))
fixture_filename = 'service_related_services_data.json'


def load_fixture(apps, schema_editor):
    fixture_file = os.path.join(fixture_dir, fixture_filename)
    call_command('loaddata', fixture_file)

def unload_fixture(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0008_auto_20150214_0022'),
    ]

    operations = [
    	migrations.RunPython(load_fixture, reverse_code=unload_fixture)
    ]
