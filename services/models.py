import itertools

from django.db import models
from django.utils.text import slugify


class Service(models.Model):

    name = models.CharField(max_length=500)
    slug = models.CharField(max_length=500)
    description = models.TextField()
    identification_guide = models.TextField()
    image_1 = models.FileField(upload_to='services/images/')
    image_2 = models.FileField(upload_to='services/images/', blank=True)
    image_3 = models.FileField(upload_to='services/images/', blank=True)
    image_4 = models.FileField(upload_to='services/images/', blank=True)
    image_5 = models.FileField(upload_to='services/images/', blank=True)
    is_featured_solution = models.BooleanField(default=False)
    featured_solution_weight = models.IntegerField(default=5)
    category = models.ForeignKey('Category')
    related_products = models.ManyToManyField('shop.Product', blank=True, null=True)
    related_services = models.ManyToManyField('self', blank=True, null=True)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        """
        Creating unique slug on save by adding an integer if it
        already exists.
        """

        self.slug = original = slugify(self.name)
        for x in itertools.count(2):
            if not Service.objects.filter(slug=self.slug).exists():
                break
            self.slug = '%s-%d' % (original, x)
        super(Service, self).save(*args, **kwargs)


class Category(models.Model):

    name = models.CharField(max_length=255)
    parent = models.ForeignKey('self', blank=True, null=True, related_name='subcategories')

    def is_child(self):
        if self.parent:
            return True
        else:
            return False

    def is_parent(self):
        return not self.is_child()

    def __unicode__(self):
        if self.parent:
            return "%s -> %s" % (self.parent.name, self.name)
        else:
            return "%s" % (self.name)

    def get_category_ids(self):
        """
        returns a list of ids for the parent id and all child ids if parent
        """
        if self.is_parent():
            cats = Category.objects.filter(parent_id=self.id)
            return [c.id for c in cats]

        else:
            return [self.id]


    @classmethod
    def get_menu(cls, current_id=0):
        """
        returns a hierarchical menu. type: list of dictionaries.
        Param: current_id - which used to determine active menu
        """

        if not current_id:
            current_id = 1


        menu_items = cls.objects.order_by('id')
        parents = [item for item in menu_items if item.is_parent()]
        children = [item for item in menu_items if item.is_child()]
        # create menu parent child relationship
        # menu = [  {'parent':parent1, 'children':[child1, child2...] },
        #           {'parent':parent2, 'children':[child3, child4...] }  ]
        active = False
        menu = []
        for p in parents:
            children_of_parent = [c for c in children if c.parent_id == p.id]
            if current_id:
                active = cls.is_menu_active(current_id, children_of_parent, p)
            menu.append({'parent':p, 'children':children_of_parent, 'active':active})
        return menu

    @classmethod
    def is_menu_active(cls,active_id, child_items, parent):
        """

        """
        active = False
        for category in child_items + [parent]:
            if int(active_id) == category.id:
                return True
        return False

    class Meta:
        verbose_name_plural = "categories"
        ordering = ['-parent_id', '-id']


