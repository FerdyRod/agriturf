from django.conf.urls import patterns, include, url
import views

urlpatterns = patterns('agriturf_supplies.services.views',

    #(r'^index/$', views.home), # main urls.py maps this directly to /
    url(r'^schedule/$', views.schedule, name='schedule'),
    url(r'^browse/$', views.search, name='search'),
    url(r'^browse/(\d+)$', views.search, name='filter'),
    url(r'^solution/(?P<slug>.*)$', views.view, name='view'),

)
