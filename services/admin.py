from django.contrib import admin
from models import Service, Category


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    exclude = ('slug',)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "parent":
            kwargs["queryset"] = Category.objects.filter(parent__isnull=True)
        return super(CategoryAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
